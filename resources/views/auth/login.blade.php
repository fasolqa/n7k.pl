@extends('auth.index')
@section('content')
<!-- <img src="/img/logo.png" alt="logo" data-src="/img/logo.png" data-src-retina="/img/logo_2x.png" width="78" height="22"> -->
<p class="p-t-15">Zaloguj się do systemu</p>
<!-- START Login Form -->
<form id="form-login" class="p-t-15" method="POST" role="form" action="{{ url('/login') }}" >
    {{ csrf_field() }}
    <div class="form-group-default form-group{{ $errors->has('email') ? ' has-error' : '' }}">
        <label>Email</label>
        <div class="controls">
            <input type="email" name="email" placeholder="uzytkownik@domena" class="form-control" value="{{ old('email') }}" autofocus>
        </div>
        @if ($errors->has('email'))
        <span class="help-block">
            <strong>{{ $errors->first('email') }}</strong>
        </span>
        @endif
    </div>
    <!-- END Form Control-->
    <!-- START Form Control-->
    <div class="form-group-default form-group{{ $errors->has('password') ? ' has-error' : '' }}">
        <label>Haslo</label>
        <div class="controls">
            <input id="password" type="password" class="form-control" placeholder="***********" name="password">
            @if ($errors->has('password'))
            <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <!-- START Form Control-->
    <div class="row">
        <div class="col-md-6 no-padding">
            <div class="checkbox ">
                <input type="checkbox" id="checkbox1" name="remember">
                <label for="checkbox1">Zapamiętaj mnie</label>
            </div>
        </div>
        <div class="col-md-6 text-right">
            <a href="/password/reset" class="text-info small">Nie pamiętasz hasła?</a>
        </div>
    </div>
    <!-- END Form Control-->
    <button type="submit" class="btn btn-primary btn-cons m-t-10">
    Zaloguj
    </button>
    <a href="/register" class="btn btn-default  btn-cons m-t-10">Zarejestruj</a>
</form>
@stop