@extends('auth.index')
<!-- Main Content -->
@section('content')
<p class="p-t-15">Zresetuj swoje hasło</p>
<form id="form-login" class="p-t-15" method="POST" role="form" action="{{ url('/login') }}" >
    @if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
    @endif
    <form role="form" method="POST" action="{{ url('/password/email') }}">
        {{ csrf_field() }}
        <div class="form-group-default form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <label for="email" class="control-label">E-Mail Address</label>
            <div class="control">
                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">
                @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
                @endif
            </div>
        </div>
        <div class="control">
            <button type="submit" class="btn btn-primary">
            Resetuj hasło
            </button>
            <a href="/login" class="btn btn-default">Wróć do logowania</a>
        </div>
    </form>
</form>
@endsection