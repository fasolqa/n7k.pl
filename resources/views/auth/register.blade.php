@extends('auth.index')
@section('content')
<p class="p-t-15">Zarejestruj się do systemu</p>
<form id="form-register" class="p-t-15" role="form" method="POST" action="{{ url('/register') }}">
	{{ csrf_field() }}
	<div class="row">
		<div class="col-sm-12">
			<div class="form-group-default form-group{{ $errors->has('name') ? ' has-error' : '' }}">
				<label for="name" class="control-label">Imię</label>
				<input id="name" type="text" class="form-control" name="name"
				value="{{ old('name') }}" autofocus>
				@if ($errors->has('name'))
				<span class="help-block">
					<strong>{{ $errors->first('name') }}</strong>
				</span>
				@endif
			</div>
		</div>
		<div class="col-sm-12">
			<div class="form-group-default form-group{{ $errors->has('email') ? ' has-error' : '' }}">
				<label for="email" class="control-label">E-Mail Address</label>
				<input id="email" type="text" class="form-control" name="email"
				value="{{ old('email') }}">
				@if ($errors->has('email'))
				<span class="help-block">
					<strong>{{ $errors->first('email') }}</strong>
				</span>
				@endif
			</div>
		</div>
		<div class="col-md-12">
			<div
				class="form-group-default form-group{{ $errors->has('password') ? ' has-error' : '' }}">
				<label for="password" class="control-label">Hasło</label>
				<input id="password" type="password" class="form-control" name="password">
				@if ($errors->has('password'))
				<span class="help-block">
					<strong>{{ $errors->first('password') }}</strong>
				</span>
				@endif
			</div>
		</div>
		<div class="col-md-12">
			<div
				class="form-group-default form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
				<label for="password-confirm" class="control-label">Powtórz hasło</label>
				<input id="password-confirm" type="password" class="form-control" name="password_confirmation">
				@if ($errors->has('password_confirmation'))
				<span class="help-block">
					<strong>{{ $errors->first('password_confirmation') }}</strong>
				</span>
				@endif
			</div>
		</div>
		<div class="col-md-12">
			<button class="btn btn-primary btn-cons m-t-10" type="submit">Stwórz nowe konto</button>
			<a href="/login" class="btn btn-default btn-cons m-t-10">Przejdź do logowania</a>
		</div>
	</div>
</form>
@stop