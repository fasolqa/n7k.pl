<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title>N7k</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <link href="/assets/css/auth/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/auth/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link class="main-stylesheet" href="/assets/css/auth/pages.min.css" rel="stylesheet" type="text/css" />
  </head>
  <body class="fixed-header ">
    <div class="login-wrapper ">
      <!-- START Login Background Pic Wrapper-->
      <div class="bg-pic">
        <!-- START Background Pic-->
        <img
        src="/assets/img/auth-bg.jpg"
        data-src="/assets/img/auth-bg.jpg"
        data-src-retina="/assets/img/auth-bg.jpg" alt="" class="lazy">
        <!-- END Background Pic-->
        <!-- START Background Caption-->
        <div class="bg-caption pull-bottom sm-pull-bottom text-white p-l-20 m-b-20">
          <h2 class="semi-bold text-white">
          Znajdź czas na nakurwianie...</h2>
          <p class="small">
            albo zostań grubaskę !!
          </p>
        </div>
        <!-- END Background Caption-->
      </div>
      <!-- END Login Background Pic Wrapper-->
      <!-- START Login Right Container-->
      <div class="login-container bg-white">
        <div class="p-l-50 m-l-20 p-r-50 m-r-20 m-t-30 sm-p-l-15 sm-p-r-15">
          <h1><strong>Nakurwiator7000</strong></h1>
          @yield('content')
          <!--END Login Form-->
          <div class="pull-bottom sm-pull-bottom">
            <div class="m-b-30 p-r-80 sm-m-t-20 sm-p-r-15 sm-p-b-20 clearfix">
              <div class="col-sm-12 no-padding m-t-10">
                <p>
                  <small>
                  Powered by <a href="http://bpcoders.pl" class="text-info"> BPCoders</a>
                  </small>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- END Login Right Container-->
      <!-- START OVERLAY --></div>
    </body>
  </html>