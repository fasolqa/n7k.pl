
<!DOCTYPE html>
<!-- Template Name: Packet - Responsive Admin Template build with Twitter Bootstrap 3.x | Author: ClipTheme -->
<!--[if IE 8]><html class="ie8" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- start: HEAD -->
<head>
    <title>Nakurwiator7000</title>
    <!-- start: META -->
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!-- end: META -->
    <!-- start: GOOGLE FONTS -->
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
    <!-- end: GOOGLE FONTS -->
    <!-- start: MAIN CSS -->
    <link rel="stylesheet" href="/assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/assets/plugins/themify-icons/themify-icons.css">
    <link rel="stylesheet" href="/assets/plugins/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="/assets/plugins/animate.css/animate.min.css">
    <link rel="stylesheet" href="/assets/plugins/perfect-scrollbar/css/perfect-scrollbar.min.css">
    <link rel="stylesheet" href="/assets/plugins/switchery/switchery.min.css">
    <link rel="stylesheet" href="/assets/plugins/seiyria-bootstrap-slider/css/bootstrap-slider.min.css">
    <link rel="stylesheet" href="/assets/plugins/ladda/ladda-themeless.min.css">
    <link rel="stylesheet" href="/assets/plugins/slick.js/slick.css">
    <link rel="stylesheet" href="/assets/plugins/slick.js/slick-theme.css">
    <link rel="stylesheet" href="/assets/plugins/toastr/toastr.min.css">
    <!-- end: MAIN CSS -->
    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    @yield('styles')
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
    <!-- start: Packet CSS -->
    <link rel="stylesheet" href="/assets/css/styles.css">
    <link rel="stylesheet" href="/assets/css/plugins.css">
    <link rel="stylesheet" href="/assets/css/themes/lyt2-theme-1.css" id="skin_color">
    <!-- end: Packet CSS -->
    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico" />
</head>
<!-- end: HEAD -->
<body>
<div id="app" class="lyt-2">
    <!-- sidebar -->
    <div class="sidebar app-aside" id="sidebar">
        <div class="sidebar-container perfect-scrollbar">
            <div>
                <!-- start: SEARCH FORM -->
                <div class="search-form hidden-md hidden-lg">
                    <a class="s-open" href="#"> <i class="ti-search"></i> </a>
                    <form class="navbar-form" role="search">
                        <a class="s-remove" href="#" target=".navbar-form"> <i class="ti-close"></i> </a>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Enter search text here...">
                            <button class="btn search-button" type="submit">
                                <i class="ti-search"></i>
                            </button>
                        </div>
                    </form>
                </div>
                <!-- end: SEARCH FORM -->
                <!-- start: USER OPTIONS -->
                <div class="nav-user-wrapper">
                    <div class="media">
                        <div class="media-left">
                            <a class="profile-card-photo" href="#">
                                <img alt="" src="/assets/images/default-user.png">
                            </a>
                        </div>
                        <div class="media-body">
                            <span class="media-heading text-white">{{ Auth::user()->name }}</span>
                            <div class="text-small text-white-transparent">
                                Początkujący grubasek
                            </div>
                        </div>
                        <div class="media-right media-middle">
                            <div class="dropdown">
                                <button href class="btn btn-transparent text-white dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-caret-down"></i>
                                </button>
                                <ul class="dropdown-menu animated fadeInRight pull-right">
                                    <li>
                                        <a href="/logout"> Wyloguj </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end: USER OPTIONS -->
                <nav>
                    <!-- start: MAIN NAVIGATION MENU -->
                    <div class="navbar-title">
                        <span>Nawigacja</span>
                    </div>
                    <ul class="main-navigation-menu">
                        <li>
                            <a href="/calendar">
                                <div class="item-content">
                                    <div class="item-media">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <div class="item-inner">
                                        <span class="title"> Kalendarz </span>
                                    </div>
                                </div> </a>
                        </li>
                        
                    </ul>
                    <!-- end: CORE FEATURES -->
                </nav>
            </div>
        </div>
    </div>
    <!-- / sidebar -->
    <div class="app-content">
        <!-- start: TOP NAVBAR -->
        <header class="navbar navbar-default navbar-static-top">
            <!-- start: NAVBAR HEADER -->
            <div class="navbar-header">
                <button href="#" class="sidebar-mobile-toggler pull-left btn no-radius hidden-md hidden-lg" class="btn btn-navbar sidebar-toggle" data-toggle-class="app-slide-off" data-toggle-target="#app" data-toggle-click-outside="#sidebar">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="/">
                    <h3 style="color: #fff; line-height: 60px;">Nakurwiator7000</h3>
                </a>
                <a class="navbar-brand navbar-brand-collapsed" href="/">
                    <h3 style="color: #fff; line-height: 60px;">N7k</h3>
                </a>

                <button class="btn pull-right menu-toggler visible-xs-block" id="menu-toggler" data-toggle="collapse" href=".navbar-collapse" data-toggle-class="menu-open">
                    <i class="fa fa-folder closed-icon"></i><i class="fa fa-folder-open open-icon"></i><small><i class="fa fa-caret-down margin-left-5"></i></small>
                </button>
            </div>
            <!-- end: NAVBAR HEADER -->
            <!-- start: NAVBAR COLLAPSE -->
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-left hidden-sm hidden-xs">
                    <li class="sidebar-toggler-wrapper">
                        <div>
                            <button href="javascript:void(0)" class="btn sidebar-toggler visible-md visible-lg">
                                <i class="fa fa-bars"></i>
                            </button>
                        </div>
                    </li>
                    <li>
                        <a href="#" class="toggle-fullscreen"> <i class="fa fa-expand expand-off"></i><i class="fa fa-compress expand-on"></i></a>
                    </li>
                </ul>

            </div>
        </header>
        <!-- end: TOP NAVBAR -->
        <div class="main-content" >
            @yield('content')
            <!-- end: EVENTS ASIDE -->
        </div>
    </div>
    <!-- start: FOOTER -->
    <footer>
        <div class="footer-inner">
            <div class="pull-left">
                &copy; <span class="current-year"></span><span class="text-bold text-uppercase"> BPCoders</span>. <span>All rights reserved</span>
            </div>
            <div class="pull-right">
                <span class="go-top"><i class="ti-angle-up"></i></span>
            </div>
        </div>
    </footer>
    <!-- end: FOOTER -->

</div>
<script src="/assets/plugins/jquery/jquery.min.js"></script>
<script src="/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="/assets/plugins/components-modernizr/modernizr.js"></script>
<script src="/assets/plugins/js-cookie/js.cookie.js"></script>
<script src="/assets/plugins/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
<script src="/assets/plugins/jquery-fullscreen/jquery.fullscreen-min.js"></script>
<script src="/assets/plugins/switchery/switchery.min.js"></script>
<script src="/assets/plugins/jquery.knobe/jquery.knob.min.js"></script>
<script src="/assets/plugins/seiyria-bootstrap-slider/bootstrap-slider.min.js"></script>
<script src="/assets/plugins/slick.js/slick.min.js"></script>
<script src="/assets/plugins/jquery-numerator/jquery-numerator.js"></script>
<script src="/assets/plugins/ladda/spin.min.js"></script>
<script src="/assets/plugins/ladda/ladda.min.js"></script>
<script src="/assets/plugins/ladda/ladda.jquery.min.js"></script>
<script src="/assets/plugins/toastr/toastr.min.js"></script>

@yield('scripts')

<script src="/assets/js/letter-icons.js"></script>
<script src="/assets/js/main.js"></script>
</body>
</html>
