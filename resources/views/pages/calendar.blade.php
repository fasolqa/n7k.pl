@extends('layouts.app')

@section('styles')
<meta name="_token" content="{{ csrf_token() }}">
<link href="/assets/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" media="screen">
<link href="/assets/plugins/eonasdan-bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
@stop

@section('content')
<div class="wrap-content container" id="container">
    <!-- start: BREADCRUMB -->
    <div class="breadcrumb-wrapper">
        <h4 class="mainTitle no-margin">Kalendarz treningów</h4>
        <ul class="pull-right breadcrumb">
            <li>
                <a href="/"><i class="fa fa-home margin-right-5 text-large text-dark"></i>Strona główna</a>
            </li>
            <li>
                Kalendarz treningów
            </li>
        </ul>
    </div>
    <!-- end: BREADCRUMB -->
    <!-- start: CALENDAR -->
    <div class="container-fluid container-fullw">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-white">
                    <div class="panel-body">
                        <div class="row">

                            <div class="col-sm-9">
                                <div id='full-calendar'></div>
                            </div>
                            <div class="col-sm-3">
                                <h4 class="space20">Wybierz typ treningu</h4>
                                <div id="event-categories">
                                @foreach ($trainings_types as $training)
                                    <div class="event-category event-generic" data-class="category-{{$training->id}}">
                                        {{$training->name}}
                                    </div>
                                @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end: CALENDAR -->
    <!-- start: EVENTS ASIDE -->
    <div class="modal fade modal-aside horizontal right events-modal"  tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog modal-md">
            <div class="modal-content">
                <form class="form-full-event">
                    <div class="modal-body">
                        <div class="form-group hidden">
                            <label> ID </label>
                            <input type="text" id="event-id">
                        </div>
                        <div class="form-group">
                            <label> Typ treningu </label>
                            <input type="text" id="event-name" disabled="disabled" placeholder="Enter title" class="form-control underline text-large" name="eventName">
                        </div>
                        <div class="form-group">
                            <label> Start </label>
                            <span class="input-icon">
													<input type="text" id="start-date-time" class="form-control underline" name="eventStartDate"/>
													<i class="ti-calendar"></i> </span>
                        </div>
                        <div class="form-group hide">
                            <label> End </label>
                            <span class="input-icon">
													<input type="text" id="end-date-time" class="form-control underline" name="eventEndDate" />
													<i class="ti-calendar"></i> </span>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-danger btn-o delete-event disabled">
                            Usuń trening
                        </button>
                        <button class="btn btn-primary btn-o save-event" type="submit">
                            Zapisz zmiany
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop

@section('scripts')
<script src="/assets/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="/assets/plugins/moment/moment-with-locales.min.js"></script>
<script src="/assets/plugins/bb-jquery-validation/jquery.validate.js"></script>
<script src="/assets/plugins/fullcalendar/fullcalendar.min.js"></script>
<script src="/assets/plugins/fullcalendar/lang/pl.js"></script>
<script src="/assets/plugins/eonasdan-bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script src="/assets/js/pages-calendar.js"></script>
<script>
    jQuery(document).ready(function() {
        Main.init();
        Calendar.init();
    });
</script>
@stop