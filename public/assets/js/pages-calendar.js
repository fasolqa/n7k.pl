var Calendar = function() {"use strict";
	var dateToShow, calendar, demoCalendar, eventClass, eventCategory, subViewElement, subViewContent, $eventDetail;
	var defaultRange = new Object;
	defaultRange.start = moment();
	defaultRange.end = moment().add(1, 'hours');
	//Calendar
	var setFullCalendarEvents = function() {
		var date = new Date();
		dateToShow = date;
		var d = date.getDate();
		var m = date.getMonth();
		var y = date.getFullYear();

	};
	//function to initiate Full Calendar
	var runFullCalendar = function() {
		$(".add-event").off().on("click", function() {
			eventInputDateHandler();
			$(".form-full-event #event-id").val("");
			$('.events-modal').modal();
		});
		$('.events-modal').on('hide.bs.modal', function(event) {
			$(".form-full-event #event-id").val("");
			$(".form-full-event #event-name").val("");
			$(".form-full-event #start-date-time").val("").data("DateTimePicker").destroy();
			$(".form-full-event #end-date-time").val("").data("DateTimePicker").destroy();
			$(".event-categories[value='job']").prop('checked', true);
		});

		$('#event-categories div.event-category').each(function() {
			// create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
			// it doesn't need to have a start or end
			var eventObject = {
				title: $.trim($(this).text()) // use the element's text as the event title
			};
			// store the Event Object in the DOM element so we can get to it later
			$(this).data('eventObject', eventObject);
			// make the event draggable using jQuery UI
			$(this).draggable({
				zIndex: 999,
				revert: true, // will cause the event to go back to its
				revertDuration: 50 //  original position after the drag
			});
		});
		/* initialize the calendar
		 -----------------------------------------------------------------*/
		var date = new Date();
		var d = date.getDate();
		var m = date.getMonth();
		var y = date.getFullYear();
		var form = '';
		$('#full-calendar').fullCalendar({
			buttonIcons: {
				prev: 'fa fa-chevron-left',
				next: 'fa fa-chevron-right'
			},
			contentHeight: 650,
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek'
			},
			events: {
				url: '/data',
				error: function(){
					alert('blad w chuj');
				}
			},
			editable: false,
			eventLimit: true, // allow "more" link when too many events
			droppable: true, // this allows things to be dropped onto the calendar !!!
			drop: function(date, allDay) {// this function is called when something is dropped

				// retrieve the dropped element's stored Event Object
				var originalEventObject = $(this).data('eventObject');

				var $category = $(this).attr('data-class');
				
				// we need to copy it, so that multiple events don't have a reference to the same object

				var newEvent = new Object;
				newEvent.title = originalEventObject.title;
				newEvent.start = new Date(date).toISOString().slice(0, 10);
				newEvent.end = moment(new Date(date)).add(1, 'hours');
				newEvent.allDay = true;
				newEvent.category = $category;
				newEvent.className = 'event-' + $category;
				console.log(newEvent);

			    $.ajaxSetup({
				    headers: {
				        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
				    }
				});
				$.ajax({
				  method: "POST",
				  url: "/add",
				  data: { type: '1', start: newEvent.start}
				})
				  .done(function( msg ) {


                      switch(msg) {
                          case 'bad date':
                              toastr.error('Nie możesz dodać treningu z przyszłą datą');
                              break;
                          case 'duplicate':
                              toastr.error('Duplikat treningu');
                              break;
                          default:
                              toastr.success('Trening został dodany');
                              console.log(msg);
                              $('#full-calendar').fullCalendar('renderEvent', newEvent, true);
                      }
				  });



				// is the "remove after drop" checkbox checked?
				if($('#drop-remove').is(':checked')) {
					// if so, remove the element from the "Draggable Events" list
					$(this).remove();
				}
			},
			selectable: false,
			selectHelper: true,
            // dayClick: function(date, jsEvent, view) {
            //     eventInputDateHandler();
            //     $(".form-full-event #event-id").val("");
            //     $(".form-full-event #event-name").val("");
            //     $(".form-full-event #start-date-time").data("DateTimePicker").date(moment(date));
            //     $(".event-categories[value='job']").prop('checked', true);
            //     $('.events-modal').modal();
            // },
			select: function(start, end, allDay) {
				eventInputDateHandler();
				$(".form-full-event #event-id").val("");
				$(".form-full-event #event-name").val("");
				$(".form-full-event #start-date-time").data("DateTimePicker").date(moment(start));
				$(".event-categories[value='job']").prop('checked', true);
				$('.events-modal').modal();
			},
			eventClick: function(calEvent, jsEvent, view) {
				eventInputDateHandler();
				var eventId = calEvent._id;
				for(var i = 0; i < demoCalendar.length; i++) {

					if(demoCalendar[i]._id == eventId) {
						$(".form-full-event #event-id").val(eventId);
						$(".form-full-event #event-name").val(demoCalendar[i].title);
						$(".form-full-event #start-date-time").data("DateTimePicker").date(moment(demoCalendar[i].start));
						if(demoCalendar[i].category == "" || typeof demoCalendar[i].category == "undefined") {
							eventCategory = "Generic";
						} else {
							eventCategory = demoCalendar[i].category;
						}

						$(".event-categories[value='" + eventCategory + "']").prop('checked', true);

					}
				}
				$('.events-modal').modal();
			}
		});
		demoCalendar = $("#full-calendar").fullCalendar("clientEvents");
	};

	var runFullCalendarValidation = function(el) {

		var formEvent = $('.form-full-event');

		formEvent.validate({
			errorElement: "span", // contain the error msg in a span tag
			errorClass: 'help-block',

			ignore: "",
			rules: {
				eventName: {
					minlength: 1,
					required: true
				},
				eventStartDate: {
					required: true,
					date: true
				},
				eventEndDate: {
					required: true,
					date: true
				}
			},
			messages: {
				eventName: "* Please specify the event title"

			},
			highlight: function(element) {
				$(element).closest('.help-block').removeClass('valid');
				// display OK icon
				$(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
				// add the Bootstrap error class to the control group
			},
			unhighlight: function(element) {// revert the change done by hightlight
				$(element).closest('.form-group').removeClass('has-error');
				// set error class to the control group
			},
			success: function(label, element) {
				label.addClass('help-block valid');
				// mark the current input as valid and display OK icon
				$(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
			},
			submitHandler: function(form) {
				var newEvent = new Object;
				newEvent.title = $(".form-full-event #event-name ").val();
				newEvent.start = new Date($('.form-full-event #start-date-time').val());
				newEvent.end = new Date($('.form-full-event #end-date-time').val());
				newEvent.category = $(".form-full-event .event-categories:checked").val();
				newEvent.className = 'event-' + $(".form-full-event .event-categories:checked").val();
				


				if($(".form-full-event #event-id").val() !== "") {
					el = $(".form-full-event #event-id").val();
					var actual_event = $('#full-calendar').fullCalendar('clientEvents', el);
					actual_event = actual_event[0];
					for(var i = 0; i < demoCalendar.length; i++) {
						if(demoCalendar[i]._id == el) {
							newEvent._id = el;
							var eventIndex = i;
						}
					}

					$('#full-calendar').fullCalendar('removeEvents', actual_event._id);
					$('#full-calendar').fullCalendar('renderEvent', newEvent, true);

					demoCalendar = $("#full-calendar").fullCalendar("clientEvents");

					

				} else {

					$('#full-calendar').fullCalendar('renderEvent', newEvent, true);
					demoCalendar = $("#full-calendar").fullCalendar("clientEvents");

				}
				$('.events-modal').modal('hide');

			}
		});
	};

	var eventInputDateHandler = function() {
		var startInput = $('#start-date-time');
		var endInput = $('#end-date-time');
		startInput.datetimepicker();
		endInput.datetimepicker();
		startInput.on("dp.change", function(e) {
			endInput.data("DateTimePicker").minDate(e.date);
		});
		endInput.on("dp.change", function(e) {
			startInput.data("DateTimePicker").maxDate(e.date);
		});
	};
	return {
		init: function() {
			setFullCalendarEvents();
			runFullCalendar();
			runFullCalendarValidation();
		}
	};
}();
