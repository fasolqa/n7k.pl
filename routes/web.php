<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Auth::routes();
Route::get('logout', function(){
    Auth::logout(); 
    return Redirect::to('/');
});
Route::get('/', 'CalendarController@index');
Route::get('/calendar', 'CalendarController@index');
Route::get('/data', 'CalendarController@data');
Route::post('/add', 'CalendarController@add');