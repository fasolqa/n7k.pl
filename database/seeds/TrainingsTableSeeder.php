<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class TrainingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('trainings')->insert([
            'user_id' => 1,
            'training_type' => 1,
            'start' => '2016-08-29 01:00:00',
            'end' => '2016-08-29 02:00:00',
        ]);
    }
}
