<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class TrainingsTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
     	DB::table('trainings_types')->insert([
 			'name' => 'Trening siłowy',
 			'color' => '#46b8da'
        ]);
    }
}
