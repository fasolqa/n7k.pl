<?php

namespace App\Http\Controllers;

use Input;

use Illuminate\Support\Facades\Auth as Auth;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;


class CalendarController extends Controller
{
 	public function __construct()
    {
         $this->middleware('auth');
    }

    public function index()
    {
    	$trainings_types = DB::table('trainings_types')->get();
        return view('pages.calendar', ['trainings_types' => $trainings_types]);
    }

    public function data()
    {
       // $trainings = DB::table('trainings')->select('training_type as title', 'id', 'start', 'end')
       // ->where('user_id', '=', Auth::id())->get();
    	$trainings = DB::table('trainings_types')
            ->leftJoin('trainings', 'trainings_types.id', '=', 'trainings.training_type')
            ->select('trainings.id', 'trainings.start', 'trainings.end', 'trainings_types.name as title')
            ->where('user_id', '=', Auth::id())
            ->get();
       return $trainings;

    }

    public function add(Request $data){
    	$type =  Input::get('type');
    	$start = Input::get('start');

        if($start > date("Y-m-d", time()+86400))
        return 'bad date';

        $training = DB::table('trainings')->where(['start', '=', $start], ['user_id', '=', Auth::id()])->count();
        if($training)
            return 'duplicate';

    	$id = DB::table('trainings')->insertGetId(
		    ['user_id' => Auth::id(), 'training_type' => $type, 'start' => $start, 'end' => $start]
		);
		return $id;



    }
}
