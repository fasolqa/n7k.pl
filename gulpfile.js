const elixir = require('laravel-elixir');
require('laravel-elixir-webpack-official');

elixir(mix => {
    mix.sass('styles.scss');
});
